package com.uyung.aplikasi_wisata;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("getwisata.php")
    Call<ListWisataModel> ambilDataWisata();

}
